import axios from "axios";

const baseUrl = 'http://lendingusa.dev.lendfoundry.com:5075/';

export function fetchAllFunctions() {
  return function(dispatch) {
    axios.get(baseUrl).
      then((response) => {
          dispatch({ type: "FETCH_FUNCTIONS_FULFILLED", payload: response.data });
      })
      .catch((err) => {
        dispatch({type: "FETCH_FUNCTIONS_REJECTED", payload: err })
      })
  }
}

export function setEditFunction(fn) {
  return function(dispatch) {
      dispatch({ type: "SET_EDITOR_FUNCTION", payload: fn });
  }
}

export function importFunction(functionsText, url = baseUrl, authorization = null) {
    return function(dispatch) {

      debugger;

      var token = authorization === null ?
        axios.defaults.headers.common['Authorization'] :
        authorization;

      var config = {
        headers: {'Authorization': token, 'Content-Type' : 'application/json' }
      };

      var functionsJSON = JSON.parse(functionsText);
      var promises = functionsJSON
        .map((fn) => {
            fn.source = fn.source.substring(1, fn.source.length-1).replace(/\\"/g, "'").replace(/(\\n|\\r)+/g, '');
            return fn;
        })
        .map((fn) => {
          fn.dependencies = Object.keys(fn.dependencies);
          return fn;
        })
        .sort((fn) => fn.dependencies === undefined ||
                      fn.dependencies === null ? 0 : fn.dependencies.length)
        .map(async(fn) => await axios.put(url, fn, config));

        debugger;
      // Promise.all(promises)
      //     .then(function(response){
      //         debugger;
      //         dispatch({type: "IMPORT_FUNCTIONS_FULFILLED", payload: {} })
      //     })
      //     .catch((err) => {
      //       debugger;
      //       dispatch({type: "IMPORT_FUNCTIONS_REJECTED", payload: err })
      //     })
    }
}

export function getConstants() {
  return  {
    baseUrl,
    token: axios.defaults.headers.common['Authorization']
  };
}

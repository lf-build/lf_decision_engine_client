import { combineReducers } from "redux"

import functions from "./functionReducer"

export default combineReducers({
  functions
})

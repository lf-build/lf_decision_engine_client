export default function reducer(state= {
  fn: {
    name: "",
    source: "",
    dependencies: { }
  },
  functions: [],
  query: "",
  fetched: false,
  fetching: false,
  imported: false,
  error: null,
}, action) {

  switch (action.type) {
    case "FETCH_FUNCTIONS": {
      return { ...state, fetching: true }
    }
    case "FETCH_FUNCTIONS_REJECTED": {
      return {...state, fetching: false, error: action.payload }
    }
    case "FETCH_FUNCTIONS_FULFILLED": {
      return {
       ...state,
       fetching: false,
       fetched: true,
       functions: action.payload,
      }
    }
    case "IMPORT_FUNCTIONS_REJECTED": {
      return {
        ...state,
        error: action.payload,
        imported: false,
      }
    }
    case "IMPORT_FUNCTIONS_FULFILLED": {
      return { ...state, imported: true }
    }
    case "SET_EDITOR_FUNCTION": {
      return  { ...state, fn: action.payload }
    }
  }

  return state;
}

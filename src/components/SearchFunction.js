require('normalize.css/normalize.css');
require('styles/App.css');

import React from 'react';
import ReactDOM from 'react-dom';

import { connect } from "react-redux"
import { searchForFunctions } from "../actions/functionActions"

@connect((store) => {
  return {
    query: store.query,
    functions: store.functions
  };
})
class SearchFunctionComponent extends React.Component {

  constructor(props) {
    super(props);
  }
  render() {

    var handleChange = function(e) {
      var query = this.refs.searchInput.value;
      //this.props.dispatch(searchForFunctions(query));
    }

    return (
          <div className="search-function form-group">
            <input className="form-control" type="text" placeholder="It's not working yet, sorry."
              onChange={ handleChange.bind(this) } ref="searchInput" />
          </div>
      );
  }

}

SearchFunctionComponent.defaultProps = {
  fns : []
};

export default SearchFunctionComponent;

require('normalize.css/normalize.css');
require('styles/App.css');

import React from 'react';
import ReactDOM from 'react-dom';

import ListFunctions from './ListFunctions'
import ExportFunctions from './ExportFunctions'
import ImportFunctions from './ImportFunctions'

import SourceEditor from './SourceEditor'
import SearchFunction from './SearchFunction'


import { Provider } from "react-redux"
import store from "../stores/functionStore.js"


class AppComponent extends React.Component {

  render() {

      return (
        <Provider store={ store }>
          <div className="row stage">
            <div className="col-md-5">
              <SearchFunction></SearchFunction>
              <div className="action-panel">
                <ImportFunctions></ImportFunctions>
                <ExportFunctions></ExportFunctions>
              </div>
              <div className="scroll list-functions">
                <ListFunctions></ListFunctions>
              </div>
            </div>
            <div className="col-md-7">
                <SourceEditor></SourceEditor>
            </div>
          </div>
        </Provider>
      )

  }

}

AppComponent.defaultProps = {
};

export default AppComponent;

require('normalize.css/normalize.css');
require('styles/App.css');

import React from 'react';
import ReactDOM from 'react-dom';

import SelectFunction from './SelectFunction'

import { connect } from "react-redux"

import { js_beautify as beautify } from "js-beautify"

@connect((store) => {
  return {
    fn: store.fn,
    functions: store.functions
  };
})
export default  class SourceEditorComponent extends React.Component {

  render() {

      const { fn, functions } = this.props;

      const mappedDependencies = Object.keys(fn.dependencies).length > 0 ?
        Object.keys(fn.dependencies).map((d) => {
          let dependency = functions.filter((f) => f.name === d ).pop();
          return(
            <span className="btn btn-default btn-dep">
              <SelectFunction fn={ dependency }></SelectFunction>
            </span>
          );
        }) : "This functions has no dependencies";

      const source = beautify(fn.source.slice(1, -1).replace(/\\/g,''));

      return (
        <div>
          <h4>Function { fn.name }</h4>
          <textarea rows="20" value={ source } ></textarea>
          <h4>Dependencies</h4>
          <div>{ mappedDependencies }</div>
        </div>
      );
  }

}

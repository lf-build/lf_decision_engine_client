require('normalize.css/normalize.css');
require('styles/App.css');

import React from 'react';
import ReactDOM from 'react-dom';

import { connect } from "react-redux"

import { Modal } from 'react-bootstrap';

@connect((store) => {
  return {
    functions: store.functions
  };
})

class ExportFunctionsComponent extends React.Component {

  componentWillMount() {
    this.setState({ showModal: false });
  }

  close() {
    this.setState({ showModal: false });
  }

  open() {
    this.setState({ showModal: true });
  }

  render() {
    return (
      <div>
        <a onClick={this.open.bind(this)}> Export </a>
        <Modal show={this.state.showModal} onHide={this.close.bind(this)}>
            <Modal.Header closeButton>
              <Modal.Title>Export functions</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <span>Copy and save the functions in the box below:</span>
              <textarea value={ JSON.stringify(this.props.functions) } rows="20"></textarea>
            </Modal.Body>
        </Modal>
      </div>
    );
  }

}

ExportFunctionsComponent.defaultProps = {
};

export default ExportFunctionsComponent;

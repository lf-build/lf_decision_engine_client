require('normalize.css/normalize.css');
require('styles/App.css');

import React from 'react';
import ReactDOM from 'react-dom';
import { connect } from "react-redux"


import { Modal } from 'react-bootstrap';
import { getConstants } from "../actions/functionActions"

import { importFunction } from "../actions/functionActions"

@connect((store) => {
  return { };
})
class ImportFunctionsComponent extends React.Component {

  componentWillMount() {
    this.setState({ showModal: false,
      importText: "",
      importURL : getConstants().baseUrl,
      importToken : getConstants().token });
  }

  close() {
    this.setState({ showModal: false });
  }

  open() {
    this.setState({ showModal: true });
  }

  handleChange(e) {
    this.setState({[e.target.name]: e.target.value});
  }

  import() {
    debugger;
    const { importText, importURL, importToken } = this.state;
    this.props.dispatch(importFunction(importText, importURL, importToken));
  }

  render() {
    return (
      <div>
        <a onClick={this.open.bind(this)}> Import </a>
        <Modal show={this.state.showModal} onHide={this.close.bind(this)}>
            <Modal.Header closeButton>
              <Modal.Title>Import functions</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <p>URL:</p>
              <div className="form-group">
                <input name="importURL" className="form-control" value={ this.state.importURL } onChange={ this.handleChange.bind(this) }></input>
              </div>
              <p>Token:</p>
              <div className="form-group">
                <input name="importToken" className="form-control" value={ this.state.importToken } onChange={ this.handleChange.bind(this) }></input>
              </div>
              <p>Paste the functions:</p>
              <textarea name="importText" dsibaled="true" rows="20" value={ this.state.importText } onChange={ this.handleChange.bind(this) }></textarea>
              <a onClick={ this.import.bind(this) } className="btn btn-success">Import</a>
            </Modal.Body>
        </Modal>
      </div>
    );
  }

}

ImportFunctionsComponent.defaultProps = {
};

export default ImportFunctionsComponent;

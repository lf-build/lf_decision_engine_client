require('normalize.css/normalize.css');
require('styles/App.css');

import React from 'react';
import ReactDOM from 'react-dom';
import SelectFunction from './SelectFunction'
import { connect } from "react-redux"

import { fetchAllFunctions } from "../actions/functionActions"

@connect((store) => {
  return {
    functions: store.functions,
    activeFunction: store.fn.name
  };
})
export default class ListFunctionsComponent extends React.Component {

  componentWillMount() {
    this.props.dispatch(fetchAllFunctions())
  }

  _checkActiveFunction (functionName) {
    return (functionName === this.props.activeFunction) ? "function-selector active" : "function-selector";
  }

  render() {
      const { functions } = this.props;

      const mappedFunctions = functions.map((fn) =>
        <li id={ fn.name } className={ this._checkActiveFunction(fn.name) }><SelectFunction fn={ fn } ></SelectFunction></li>
      );

      return (
        <div>
          <ul>
            { mappedFunctions }
          </ul>
        </div>
      );
  }

}

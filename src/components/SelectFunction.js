require('normalize.css/normalize.css');
require('styles/App.css');

import React from 'react';
import ReactDOM from 'react-dom';

import { connect } from "react-redux"
import { setEditFunction } from "../actions/functionActions"

import { goToAnchor } from 'react-scrollable-anchor'
import { configureAnchors } from 'react-scrollable-anchor'

@connect((store) => {
  return { };
})
class SelectFunctionComponent extends React.Component {

  handleClick(e) {
    this.props.dispatch(setEditFunction(this.props.fn));
    configureAnchors({offset: -60, scrollDuration: 200})
    goToAnchor(this.props.fn.name, false);
  }

  render() {
      return (
          <span onClick={this.handleClick.bind(this)}>{ this.props.fn.name } </span>
      );
  }

}

SelectFunctionComponent.defaultProps = {
  "fn" : { name: "Loading.." }
};

export default SelectFunctionComponent;
